import logging
import sys

from gameserver.config import loop, server_info
from gameserver.protocol.outer import Lineage2GameProtocol
from gameserver.manager import GameServerPacketManager

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

LOG = logging.getLogger("L2py-server-game")


async def main(host, port):
    server = await loop.create_server(
        lambda: Lineage2GameProtocol(loop, GameServerPacketManager),
        host, port)

    LOG.info("Starting L2 game server on %s:%s.", host, port)
    async with server:
        await server.serve_forever()


loop.run_until_complete(main(*server_info))
