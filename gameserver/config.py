import apscheduler.schedulers.asyncio
import apscheduler.executors.asyncio
import apscheduler.jobstores.memory
import asyncio
import os

loop = asyncio.get_event_loop()

# Server config
SERVER_ID = os.environ.get("SERVER_ID", 1)
SERVER_HOST = os.environ.get("SERVER_HOST", "0.0.0.0")
SERVER_PORT = os.environ.get("SERVER_PORT", 7777)
server_info = (SERVER_HOST, SERVER_PORT)

# Login server info
LOGIN_SERVER_API_IP = os.environ.get("LOGIN_SERVER_API_IP", "localhost")
LOGIN_SERVER_API_PORT = os.environ.get("LOGIN_SERVER_API_PORT", 2107)
login_server_connection_info = (LOGIN_SERVER_API_IP, LOGIN_SERVER_API_PORT)

# Data server info
DATA_SERVER_API_IP = os.environ.get("DATA_SERVER_API_IP", "localhost")
DATA_SERVER_API_PORT = os.environ.get("DATA_SERVER_API_PORT", 2108)
data_server_connection_info = (DATA_SERVER_API_IP, DATA_SERVER_API_PORT)

# Event scheduler
scheduler_job_store = {
    "default": apscheduler.jobstores.memory.MemoryJobStore()
}
scheduler_executors = {
    "default": apscheduler.executors.asyncio.AsyncIOExecutor()
}

scheduler = apscheduler.schedulers.asyncio.AsyncIOScheduler(jobstores=scheduler_job_store,
                                                            executors=scheduler_executors)
