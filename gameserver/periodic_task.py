import functools
from gameserver.config import scheduler


def async_periodic_task(call_args=None, call_kwargs=None, **job_kwargs):
    jobs = {}

    def inner(func):
        @functools.wraps(func)
        async def wrap(*args, **kwargs):
            return await func(*args, **kwargs)

        if func.__name__ not in jobs:
            jobs[func.__name__] = func
            scheduler.add_job(inner(func), "interval", args=call_args, kwargs=call_kwargs,
                              **job_kwargs)
        return wrap

    return inner


scheduler.start()
