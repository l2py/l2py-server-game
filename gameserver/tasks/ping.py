from gameserver.periodic_task import async_periodic_task
from gameserver.config import SERVER_ID, data_server_connection_info
from common.client.data_client import DataClient

client = DataClient(*data_server_connection_info)


@async_periodic_task(seconds=10)
async def ping_dataserver():

    await client.post_gameserver_ping(SERVER_ID)
