from gameserver.keys.xor import GameXorKey
from gameserver.state import Connected


class GameClient:
    def __init__(self, protocol):
        self.state = Connected()
        self.protocol = protocol
        self.xor_key = GameXorKey()
        self.encryption_enabled = False
        self.session_id = None
        self.blowfish_enabled = False
