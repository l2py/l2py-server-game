FROM python:3.8
ADD gameserver /code/l2py_game/gameserver
ADD requirements.txt /code/l2py_game/
ADD setup.py /code/l2py_game/
WORKDIR /code/l2py_game/
RUN pip install -r requirements.txt
RUN python setup.py install
ENV PYTHONBUFFERED 1
CMD python gameserver/runner.py
